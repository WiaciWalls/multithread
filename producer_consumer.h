#pragma once

#include <string>

int run_threads(int thread_count, int upper_sleep_limit, bool debug_mode, std::string input);
