#include <string>
#include <cstring>
#include <iostream>
#include "producer_consumer.h"

int main(int c, char** v) {
    int thread_count, upper_sleep_limit;
    bool debug_mode = false;
    if (c == 3) {
        thread_count = std::stoi(v[1]);
        upper_sleep_limit = std::stoi(v[2]);
    } else if (c == 4 && std::strcmp(v[1], "-d") == 0) {
        thread_count = std::stoi(v[2]);
        upper_sleep_limit = std::stoi(v[3]);
        debug_mode = true;
    } else {
        std::cout << "Format: [-d] thread_count upper_sleep_limit" << std::endl;
        return -1;
    }

    if (thread_count <= 0 || upper_sleep_limit < 0) {
        std::cout << "thread_count must be > 0, upper_sleep_limit must be >= 0" << std::endl;
        return -1;
    }

    std::string input;
    std::getline(std::cin, input);
    std::cout << run_threads(thread_count, upper_sleep_limit, debug_mode, input) << std::endl;
}