#include "producer_consumer.h"
#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

#define BUFFER_SIZE 16

struct buffer {
    int buffer[BUFFER_SIZE];
    int length;
    pthread_mutex_t mutex;
    pthread_cond_t produce_cond, consume_cond;
};

struct cons_args {
    int upper_sleep_limit;
    bool debug_mode;
    bool* running;
    buffer* buffer;
};

struct prod_args {
    buffer* buffer{};
    std::string input;
};

struct int_args {
    pthread_mutex_t mutex{};
    bool running{};
    std::vector<pthread_t> consumer_threads;
};

int get_tid() {
    char address[16];
    pthread_getname_np(pthread_self(), address, 16);
    int* temp = (int*)address;
    return *temp;
}

void* producer_routine(void* arg) {
    auto* args = (prod_args*)arg;
    std::string string_number;
    std::stringstream split(args->input);
    while (std::getline(split, string_number, ' ')) {
        int number = std::stoi(string_number);
        pthread_mutex_lock(&args->buffer->mutex);
        while (args->buffer->length == BUFFER_SIZE) {
            pthread_cond_wait(&args->buffer->produce_cond, &args->buffer->mutex);
        }
        args->buffer->buffer[args->buffer->length] = number;
        args->buffer->length++;
        pthread_cond_signal(&args->buffer->consume_cond);
        pthread_mutex_unlock(&args->buffer->mutex);
    }
    return nullptr;
}

void* consumer_routine(void* arg) {
    auto* args = (cons_args*)arg;
    while (*args->running && args->buffer->length == 0) {
        usleep(10);
    }
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, nullptr);
    thread_local auto* thread_sum = new int(0);

    while (*args->running || args->buffer->length > 0) {
        pthread_mutex_lock(&args->buffer->mutex);
        while (args->buffer->length == 0) {
            if (!*args->running) {
                pthread_mutex_unlock(&args->buffer->mutex);
                pthread_cond_signal(&args->buffer->consume_cond);
                return thread_sum;
            }
            pthread_cond_wait(&args->buffer->consume_cond, &args->buffer->mutex);
        }
        args->buffer->length--;
        *thread_sum += args->buffer->buffer[args->buffer->length];
        if (args->debug_mode) {
            std::cout << "Tid: " << get_tid() << ". Sum: " << *thread_sum << std::endl;
        }

        pthread_cond_signal(&args->buffer->produce_cond);
        pthread_mutex_unlock(&args->buffer->mutex);

        if (args->upper_sleep_limit != 0) {
            usleep(rand() % (args->upper_sleep_limit * 1000));
        }
    }
    return thread_sum;
}

void* consumer_interruptor_routine(void* arg) {
  auto* args = (int_args*)arg;
  while (true) {
    pthread_mutex_lock(&args->mutex);
    if (!args->running) {
      break;
    }
    pthread_mutex_unlock(&args->mutex);
    int thread_id_to_stop = rand() % args->consumer_threads.size();
    pthread_cancel(args->consumer_threads[thread_id_to_stop]);
  }
  pthread_mutex_unlock(&args->mutex);

  return nullptr;
}

int run_threads(int thread_count, int upper_sleep_limit, bool debug_mode, std::string input) {
    buffer buffer{};
    buffer.length = 0;
    buffer.mutex = PTHREAD_MUTEX_INITIALIZER;
    buffer.produce_cond = PTHREAD_COND_INITIALIZER;
    buffer.consume_cond = PTHREAD_COND_INITIALIZER;

    bool running = true;

    std::vector<int> thread_ids;
    thread_ids.reserve(thread_count);

    std::vector<cons_args> args;
    args.reserve(thread_count);

    std::vector<pthread_t> consumer_threads;
    consumer_threads.reserve(thread_count);

    for (int i = 0; i < thread_count; ++i) {
        thread_ids.push_back(i + 1);
        consumer_threads.push_back(0);
        cons_args arg{};
        arg.buffer = &buffer;
        arg.debug_mode = debug_mode;
        arg.upper_sleep_limit = upper_sleep_limit;
        arg.running = &running;
        args.push_back(arg);

        pthread_create(&consumer_threads[i], nullptr, consumer_routine, &args[i]);
        char* address = (char*)&thread_ids[i];
        pthread_setname_np(consumer_threads[i], address);
    }

    prod_args prod_arg;
    prod_arg.buffer = &buffer;
    prod_arg.input = std::move(input);
    pthread_t producer_thread;
    pthread_create(&producer_thread, nullptr, producer_routine,&prod_arg);

    pthread_t interruptor_thread;
    int_args interruptor_arguments;
    interruptor_arguments.consumer_threads = consumer_threads;
    interruptor_arguments.running = true;
    interruptor_arguments.mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_create(&interruptor_thread, nullptr, consumer_interruptor_routine, &interruptor_arguments);

    pthread_join(producer_thread, nullptr);
    pthread_mutex_lock(&interruptor_arguments.mutex);
    interruptor_arguments.running = false;
    pthread_mutex_unlock(&interruptor_arguments.mutex);
    pthread_join(interruptor_thread, nullptr);

    void* thread_sum;
    int sum = 0;
    pthread_mutex_lock(&buffer.mutex);
    running = false;
    pthread_mutex_unlock(&buffer.mutex);
    for (int i = 0; i < thread_count; i++) {
        pthread_cond_signal(&(&buffer)->consume_cond);
        pthread_join(consumer_threads[i], &thread_sum);
        sum += *(int*)thread_sum;
    }

    return sum;
}
